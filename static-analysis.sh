#!/bin/bash

echo "Running PHP CS Fixer" \
&& vendor/bin/phpcbf --standard=dev/tests/static/framework/Magento app/code vendor/pim vendor/ggg

echo "Running PHPCS" \
&& vendor/bin/phpcs -n --standard=dev/tests/static/framework/Magento app/code vendor/pim vendor/ggg \
&& echo "Running PHPMD" \
&& vendor/bin/phpmd app/code text phpmd.xml \
&& vendor/bin/phpmd vendor/ggg text phpmd.xml \
&& vendor/bin/phpmd vendor/pim text phpmd.xml \
&& echo "Running PHPStan" \
&& vendor/bin/phpstan analyse --level=8 --configuration=phpstan.neon app/code vendor/ggg vendor/pim \
&& echo "Running Coverage Code" \
&& php -d xdebug.mode=coverage vendor/phpunit/phpunit/phpunit -c phpunit.xml --coverage-html coverage
