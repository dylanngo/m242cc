# Change Log


## [Unreleased] - yyyy-mm-dd

Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.

### Added
- [GGG-100](http://tickets.projectname.com/browse/GGG-100)
  MINOR Ticket title goes here.
- [GGG-101](http://tickets.projectname.com/browse/GGG-101)
  PATCH Ticket title goes here.


## [1.0.0] - 2022-02-12

- [GGG-102](http://tickets.projectname.com/browse/GGG-102)
  Update Gitlab CI/CD support auto deploy to staging environment.
- [GGG-103](http://tickets.projectname.com/browse/PGGG-103)
  PATCH Add logic to runsheet teaser delete to delete corresponding
  schedule cards.
