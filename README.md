# Change Log


## [Unreleased] - yyyy-mm-dd

Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.

## [1.0.4] - 2022-02-22

- [GGG-1002](http://tickets.projectname.com/browse/GGG-102)
  Update Gitlab CI/CD support auto deploy to staging environment.

## [1.0.3] - 2022-02-22

- [GGG-1001](http://tickets.projectname.com/browse/GGG-102)
  Update Gitlab CI/CD support auto deploy to staging environment.
- [GGG-109](http://tickets.projectname.com/browse/PGGG-103)
  PATCH Add logic to runsheet teaser delete to delete corresponding
  schedule cards.

## [1.0.2] - 2022-02-14

- [GGG-107](http://tickets.projectname.com/browse/GGG-102)
  Update Gitlab CI/CD support auto deploy to staging environment.
- [GGG-108](http://tickets.projectname.com/browse/PGGG-103)
  PATCH Add logic to runsheet teaser delete to delete corresponding
  schedule cards.

## [1.0.0] - 2022-02-12

- [GGG-102](http://tickets.projectname.com/browse/GGG-102)
  Update Gitlab CI/CD support auto deploy to staging environment.
- [GGG-103](http://tickets.projectname.com/browse/PGGG-103)
  PATCH Add logic to runsheet teaser delete to delete corresponding
  schedule cards.
